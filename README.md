########################################################################
##                      Machine learning plugins                      ##
########################################################################


# Folders

## User Folders

Each user on the project has a folder for their own development. You may
store whaever you like here, plugins and test configs in particular.

## Config and plugin folders

These are sample configs and plugins.

# Using the plugins

- Clone this repository so that it is located in
  [zeomine_root]/user_data/plugins/
- Copy the "news.yml" file to the user_data/configs file, and
  edit the NewsAnalysis plugin to point to your plugin's location

# Gathering Data

Make sure at least one of the link types (internal, external, file) is
not in the exclude_type list. After this, you may set a maximum number
of each link type to crawl (news.yml), or set to 0 to crawl all found
links.

# Processing existing data

If you set the "exclude_type" to have internal, external, and file links
excluded, then the crawler skip crawling, so that you can do data
analysis with existing data.
