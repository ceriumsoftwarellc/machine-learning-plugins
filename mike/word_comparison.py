## Word Comparison
#
# Creates a comparison chart of word counts between two different 
# Zeomine runs. This will compute both a correlation and difference 
# report.
#
class WordComparison():
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  z_obj = None

  # Settings dict
  settings = {}

  # run_ids is a list of run IDs of the Zeomine instances you want to 
  # perform the analysis on. This should be a list with two UUIDs.
  settings['run_ids'] = []

  # Local data dict
  data = {'first': {}, 'second': {}, 'compare': {}}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ###########
  # Common data functions


  ##############################################################################
  # Zeomine plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.z_obj.conf['plugins'] and self.z_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.z_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          self.settings[section] = conf[section]
    self.z_obj.pprint('Successfully loaded WordComparison.', 2, 2)

  ## Initiate Plugin
  #
  # 
  def initiate(self):
    self.z_obj.excom('CREATE TABLE IF NOT EXISTS word_comparison (zeomine_instance text, zeomine_run_1 text, zeomine_run_2 text, word text, fraction_1 float, fraction_2 float, factor float, normalized_factor float)')
    self.z_obj.excom('CREATE TABLE IF NOT EXISTS word_comparison_correlation (zeomine_instance text, zeomine_run_1 text, zeomine_run_2 text, signature_correlation float, coincidence_correlation float)')

  ## Generate Reports and save data
  #
  # 
  def report(self):
    if len(self.settings['run_ids']) == 2 and isinstance(self.settings['run_ids'][0], int) and isinstance(self.settings['run_ids'][1], int):
      text_data_1 = self.z_obj.fetchall('select word,normalized_fraction from word_count where zeomine_instance=? and url=?', (self.settings['run_ids'][0], 0))
      text_data_2 = self.z_obj.fetchall('select word,normalized_fraction from word_count where zeomine_instance=? and url=?', (self.settings['run_ids'][1], 0))
      if text_data_1 and text_data_2:
        text_data = {'first': text_data_1, 'second': text_data_2}
        # Populate data with the list of first and second items
        for tset in text_data:
          for t in text_data[tset]:
            self.data[tset][t[0]] = t[1]
        # Create the comparison table
        for t in self.data['first']:
          if t in self.data['second']:
            self.data['compare'][t] = {'fraction_1': self.data['first'][t], 'fraction_2': self.data['second'][t]}
            self.data['compare'][t]['factor'] = self.data['first'][t]/self.data['second'][t]
            if self.data['first'][t] >= self.data['second'][t]:
              self.data['compare'][t]['normalized_factor'] = self.data['second'][t]/self.data['first'][t]
            else:
              self.data['compare'][t]['normalized_factor'] = self.data['compare'][t]['factor']
          else:
            self.data['compare'][t] = {'fraction_1': self.data['first'][t], 'fraction_2': 0, 'factor': 0, 'normalized_factor': 0}
        # Grab all items in the second run that didn't match with the first.
        # All 2-1 matches are 1-2 matches, and therefore have already been found.
        for t in self.data['second']:
          if t in self.data['compare']:
            pass
          else:
            self.data['compare'][t] = {'fraction_1': 0, 'fraction_2': self.data['second'][t], 'factor': 0, 'normalized_factor': 0}
        
        # Insert the compare data into the table
        for t in self.data['compare']:
          d = self.data['compare'][t]
          insert = (self.z_obj.run_id, self.settings['run_ids'][0], self.settings['run_ids'][1], t, d['fraction_1'], d['fraction_2'], d['factor'], d['normalized_factor'])
          self.z_obj.ex('insert into word_comparison values(?,?,?,?,?,?,?,?)',(insert))
        self.z_obj.com()
        
        # Compute Correlation
        signature_factor_total = 0
        coincidence_factor_total = 0
        for t in self.data['compare']:
          signature_factor_total += self.data['compare'][t]['normalized_factor']
          if self.data['compare'][t]['normalized_factor']:
            coincidence_factor_total += 1
        factor_count = len(self.data['compare'])
        signature_correlation = (signature_factor_total/factor_count)**2
        coincidence_correlation = (coincidence_factor_total/factor_count)**2
        self.z_obj.excom('insert into word_comparison_correlation values(?,?,?,?,?)',(self.z_obj.run_id,self.settings['run_ids'][0],self.settings['run_ids'][1], signature_correlation, coincidence_correlation))

  ## Final actions for Zeomine shutdown
  #
  # 
  def shutdown(self):
    pass

  ##############################################################################
  # Core Crawler - Selenium plugins
  ##############################################################################

  ## Core Selenium Crawler callback
  #
  # 
  def core_crawler_selenium(self, current_url):
    pass
