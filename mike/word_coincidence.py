## Word Coincidence
#
# Creates a count of words that coincide with a search term. The count
# report is a count of the number of times a word appears with the term,
# and the percentage of times the word appears with the term. This 
# looks at all URLs in a Zeomine instance, so larger runs will produce 
# higher counts. Text must be found from the text_extraction plugin.
#
# A coincident word is counted once per URL.
#
class WordCoincidence():
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  z_obj = None

  # Settings dict
  settings = {}

  # Search terms - Dict with search keys, and array of applicable search
  # strings, i.e. {'search': ['search', 'searching']}
  # TODO add regex
  settings['search'] = {}

  # run_id is the run ID of the Zeomine instance you want to perform the 
  # analysis on. Set to the integer value, 'current' to get the current run,
  # or 'previous' to get the previous run.
  settings['run_id'] = 'current'

  # The selector from text_extraction, or False if you want to include everything.
  settings['selector'] = False

  # List of words to not include in our report.
  settings['blacklist'] = [] # Example: ['a', 'an', 'the']

  # List of substrings to remove from found words, usually punctuation
  settings['cleanup_list'] = [':', ';', '--', '?', '&', '.', '!', ',', '"', '”', '“', '‘','’', '(', ')']

  # Minimum length of words to include
  settings['min_length'] = 1

  # Local data dict
  data = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ###########
  # Common data functions

  # Check against whitelist: item contains a matching substring
  def in_whitelist(self, item, whitelist):
    if not whitelist:
      return True
    else:
      for substr in whitelist:
        if substr in item:
          return True
      return False

  # Check against blacklist: item does not contain a matching substring
  def in_blacklist(self, item, blacklist):
    if not blacklist:
      return False
    else:
      for substr in blacklist:
        if substr in item:
          return True
      return False
  
  # Text Cleaner
  def clean_text(self, text):
    ret = []
    #cleanup_list = [':', '-', '?', '&']
    for t in text:
      for c in self.settings['cleanup_list']:
        t = t.replace(c, '')
      if t:
        if len(t) >= self.settings['min_length']:
          if not self.in_blacklist(t, self.settings['blacklist']):
            ret.append(t)
    return ret

  ##############################################################################
  # Zeomine plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.z_obj.conf['plugins'] and self.z_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.z_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          self.settings[section] = conf[section]
    # Set up searach terms in data
    for search in self.settings['search']:
      self.data[search] = {'urls': [] ,'coincident_words': {}}
    self.z_obj.pprint('Successfully loaded WordCoincidence.', 2, 2)

  ## Initiate Plugin
  #
  # 
  def initiate(self):
    self.z_obj.excom('CREATE TABLE IF NOT EXISTS word_coincidence (zeomine_instance text, zeomine_run text, search text, coincident_word text, count int, fraction float, normalized_fraction float)')
    if self.settings['run_id'] == 'current':
      self.settings['run_id'] = self.z_obj.run_id
    elif self.settings['run_id'] == 'previous':
      self.settings['run_id'] = self.z_obj.run_id - 1

  ## Generate Reports and save data
  #
  # 
  def report(self):
    if self.settings['selector']:
      text_data = self.z_obj.fetchall('select url,extracted_text from text_extraction where zeomine_instance=? and selector=?', (self.settings['run_id'],self.settings['selector']))
    else:
      text_data = self.z_obj.fetchall('select url,extracted_text from text_extraction where zeomine_instance=?', (self.settings['run_id'],))
    # Perform search
    for tdata in text_data:
      for search in self.settings['search']:
        if tdata[0] not in self.data[search]['urls']:
          for term in self.settings['search'][search]:
            if term in str(tdata[1]):
              self.data[search]['urls'].append(tdata[0])
    # Calculate word counts
    for tdata in text_data:
      td = tdata[1].strip("'").split(' ')
      td = self.clean_text(td)
      for search in self.settings['search']:
        if tdata[0] in self.data[search]['urls']:
          for t in td:
            tl = t.lower()
            if tl in self.data[search]['coincident_words']:
              self.data[search]['coincident_words'][tl] += 1
            else:
              self.data[search]['coincident_words'][tl] = 1
          

    # Create reports
    for search in self.settings['search']:
      if self.data[search]['coincident_words']:
        max_word = max(self.data[search]['coincident_words'], key=self.data[search]['coincident_words'].get)
        max_word_count = self.data[search]['coincident_words'][max_word]
        total = sum(self.data[search]['coincident_words'].values())
        for word in sorted(self.data[search]['coincident_words'], key=self.data[search]['coincident_words'].get, reverse=True):
          count = self.data[search]['coincident_words'][word]
          frac = self.data[search]['coincident_words'][word] / total
          norm = self.data[search]['coincident_words'][word] / max_word_count
          self.z_obj.ex('insert into word_coincidence values (?,?,?,?,?,?,?)', (self.z_obj.run_id,self.settings['run_id'],search,word,count,frac,norm))
      self.z_obj.com()

  ## Final actions for Zeomine shutdown
  #
  # 
  def shutdown(self):
    pass

  ##############################################################################
  # Core Crawler - Selenium plugins
  ##############################################################################

  ## Core Selenium Crawler callback
  #
  # 
  def core_crawler_selenium(self, current_url):
    pass
