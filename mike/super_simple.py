## Sample Plugin
#
class SuperSimplePlugin():
  ##############################################################################
  # Plugin Settings
  ##############################################################################
  
  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  z_obj = None
  
  # Settings dict
  settings = {}
  
  settings['drop_first'] = True

  # Local data dict
  data = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ###########
  # Common data functions

  # Check against whitelist: item contains a matching substring
  def in_whitelist(self, item, whitelist):
    if not whitelist:
      return True
    else:
      for substr in whitelist:
        if substr in item:
          return True
      return False

  # Check against blacklist: item does not contain a matching substring
  def in_blacklist(self, item, blacklist):
    if not blacklist:
      return False
    else:
      for substr in blacklist:
        if substr in item:
          return True
      return False

  ##############################################################################
  # Zeomine plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.z_obj.conf['plugins'] and self.z_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.z_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          self.settings[section] = conf[section]
    # Run additonal actions specific to your plugin
    self.z_obj.pprint("Successfully loaded SuperSimplePlugin.", 0)

  ## Initiate Plugin
  #
  # 
  def initiate(self):
    # delete earlier table, as we make a new one each time.
    if self.settings['drop_first']:
      self.z_obj.excom('DROP TABLE super_simple')
    self.z_obj.excom('CREATE TABLE IF NOT EXISTS super_simple (zeomine_instance text, url text, status_code int)')

  ## Generate Reports and save data
  #
  # 
  def report(self):
    results = self.z_obj.fetchall('select zeomine_instances.description, urls.url , crawler_basic_data.status from crawler_basic_data \
      inner join zeomine_instances on zeomine_instances.uuid = crawler_basic_data.zeomine_instance \
      inner join urls on urls.rowid = crawler_basic_data.url')
    for result in results:
      self.z_obj.excom('insert into super_simple values(?,?,?)',result)
