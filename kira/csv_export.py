import json
import requests
import csv

## Spectra Reporter
#
# Send data to a Spectra Server for storage and sharing. 
#
# Use SpectraLoader to retrieve data from a server.
#
class CSVExport():
  ##############################################################################
  # Plugin Settings
  ##############################################################################

  # Define the parent ZSM object so we can call its functions and access its 
  # data. Set this in load_config
  z_obj = None

  # Settings dict
  settings = {}

  # Output file
  settings['output_file_name'] = 'output.csv'
  # 'a' to append new data, 'w' to overwrite
  settings['output_file_mode'] = 'a'

  # Queries for getting spectra data
  # List of strings
  # Example:
  # query = ['select selector,extracted_text from text_extraction where selector="h1" and zeomine_instance=uuid;']
  settings['queries'] = []
  

  # Local data dict
  data = {}

  ##############################################################################
  # Helper Functions
  ##############################################################################

  ##############################################################################
  # Zeomine plugins
  ##############################################################################

  ## Add configs to this module from parent object
  #
  def load_config(self):
    # If we have settings, load them.
    if self.__class__.__name__ in self.z_obj.conf['plugins'] and self.z_obj.conf['plugins'][self.__class__.__name__]:
      conf = self.z_obj.conf['plugins'][self.__class__.__name__]['settings']
      for section in conf:
        if isinstance(conf[section], dict):
          for subsection in conf[section]:
            if section in self.settings:
              self.settings[section][subsection] = conf[section][subsection]
        # If not a dict, assume the section is a single property to set.
        else:
          self.settings[section] = conf[section]
    self.z_obj.pprint('Successfully loaded SpectraReporter.', 2, 2)

  ## Initiate Plugin
  #
  # 
  def initiate(self):
    pass

  ## Generate Reports and save data
  #
  # 
  def report(self):
    # Run through each query item
    for query in self.settings['queries']:
      # Do the query
      if '[current]' in query:
        query = query.replace('[current]', self.z_obj.run_id)
      data = self.z_obj.fetchall(query)
      csv_path = self.z_obj.settings['user_data']['base'] + self.z_obj.settings['user_data']['data'] + self.settings['output_file_name']
      with open(csv_path, self.settings['output_file_mode']) as f:
        writer = csv.writer(f)
        for d in data:
          l = list(d)
          writer.writerow(l)
  
  ## Final actions for Zeomine shutdown
  #
  # 
  def shutdown(self):
    pass
